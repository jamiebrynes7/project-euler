def parseData() :
    p = 'data.txt'
    try :
        f = open(p)
    except IOError:
        print 'Cannot find file: ' + p
        sys.exit(1)

    data = []
    for line in f :
        data.append(map(int ,line.strip('\n').split(' ')))

    return data

def simplifyLayer(data):
    size = len(data)
    last_layer = data[size-1]
    next_layer = data[size-2]

    for i,item in enumerate(next_layer) :

        if last_layer[i] > last_layer[i+1] :
            next_layer[i] += last_layer[i]
        else :
            next_layer[i] += last_layer[i+1]

    data[size-2] = next_layer
    del data[size-1]

    return data

def printData(data) :

    for layer in data :
        s = ''
        for item in layer :
            s += str(item) + ' '

        print s


#Start of script
data = parseData()

while(len(data) > 1) :
    printData(data)
    simplifyLayer(data)

print data
