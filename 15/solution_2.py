import time
dim = 20

def solution(array_size) :

    L = [1] * array_size

    for i in range(array_size) :
        for j in range(i) :
            L[j] = L[j] + L[j-1]
            print L
        L[i] = 2 * L[i-1]

    print L

solution(dim)
