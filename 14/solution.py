
def sequenceLength(num) :

    i = 0
    while(num > 1) :

        if num % 2 == 0 :
            num = num / 2
        else :
            num = 3 * num +1

        i+= 1

    return i

max_chain = 0
num_max = 0

print sequenceLength(13)

for num in range(0,1000000) :

    chain = sequenceLength(num)

    if chain > max_chain :
        max_chain = chain
        num_max = num




print num_max
print max_chain
