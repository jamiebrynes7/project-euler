import string
def nameScore(name) :

    score = 0
    for letter in name :
        score += letterScore(letter)

    return score

#NOTE: To make this more efficient, could build a dictionary of letters -> score. Reduces lookups.
def letterScore(letter) :

    for i in range(0,len(string.ascii_uppercase)) :
        if letter == string.ascii_uppercase[i] :
            return i+1

p = 'data.txt'

try :
    f = open(p)
except IOError:
    print 'Error opening data file.'

line = f.readline()

names = sorted(line.replace('"','').split(','))

total = 0

for i,name in enumerate(names) :
    total += (i+1) * nameScore(name)

print total
