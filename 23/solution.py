
def abundant(number) :

    divs = []
    #First find divisors
    for n in range(1,number) :
        if number % n == 0 :
            divs.append(n)

    if sum(divs) > number :
        return True

    return False

max_iter = 28123
abundants = []
#First find all abundant numbers.. only need to check up to max_iter.
for i in range(1,max_iter + 1) :
    print i
    if abundant(i) :
        abundants.append(i)

nums = []
for i in range(0,len(abundants)) :
    for j in range(i, len(abundants)) :
        print i, j
        i_s = abundants[i] + abundants[j]
        if i_s > max_iter :
            break
        if i_s not in nums :
            nums.append(i_s)
total_sum = sum(x for x in range(1,28123 + 1))
print total_sum - sum(nums)
