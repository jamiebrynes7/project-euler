data_p = 'data.txt'
digits = 50

try :
    data_f = open(data_p, 'r')
except IOError :
    print 'Error opening file at: ' + data_p
    sys.exit(1)

data = []
for line in data_f :
    data.append(line.strip('\n'))

tsum = sum([int(i) for i in data])

print tsum

print str(tsum)[0:10]
