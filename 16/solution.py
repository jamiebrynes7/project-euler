import math
import time

s = '1'

for i in range(1000) :
    carry  = 0
    new_s = ''
    for i in range(len(s)-1, -1, -1) :
        c = int(s[i])
        value = 2 * c + carry
        carry = (value - value % 10) / 10
        value = value % 10
        new_s = str(value) + new_s

    if carry != 0 :
        new_s = str(carry) + new_s

    s = new_s

strsum = 0
for c in s :
    strsum += int(c)

print strsum
