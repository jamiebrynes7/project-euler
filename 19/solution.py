year = 1900

months = {}
months['Jan'] = 31
months['Feb'] = 28
months['Mar'] = 31
months['Apr'] = 30
months['May'] = 31
months['Jun'] = 30
months['Jul'] = 31
months['Aug'] = 31
months['Sept'] = 30
months['Oct'] = 31
months['Nov'] = 30
months['Dec'] = 31

months_iter = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec']
days_iter = ['Mon', 'Tues', 'Wed', 'Thurs', 'Fri', 'Sat', 'Sun']

#Script
day = 0
count = 0
for year in range(1900, 2001) :
    if year % 4 == 0:
        if year % 100 == 0 :
            if year % 400 == 0:
                months['Feb'] = 29
        else :
            months['Feb'] = 29
    else :
        months['Feb'] = 28

    for month in months_iter :
        for day_num in range(0,months[month]) :
            day+= 1
            if day_num == 0 and day % 7 == 6 :
                #First of month is Sunday
                 if year != 1900 :
                     count += 1
                     print day_num, month, year

print count
