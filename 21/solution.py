#First construct the d-set

def findD(num) :

    d = 0

    for i in range(1,num) :

        if num % i == 0 :
            d += i

    return d

D = {}
for i in range(1,10000) :
    D[str(i)] = str(findD(i))

su = 0
for key,value in D.iteritems() :
    for key_2,value_2 in D.iteritems():
        if key != key_2 :
            if key == value_2 and key_2 == value:
                su += (int(key) + int(value_2))

#Pairs will be picked up twice --> divide by two.
print su/2
